import React, {Component} from 'react';
import {Image, View, Text, StyleSheet, SafeAreaView, TouchableOpacity} from 'react-native';

export default class MyHomeScreen extends Component {
    static navigationOptions = {
        drawerLabel: 'Home',
        drawerIcon: () => (
            <Image
                source={require('../../assets/home.png')}
                style={[styles.icon]}
            />
        ),
    };

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: 'pink',
                }}
            >
                <TouchableOpacity
                    onPress={()=> this.props.navigation.navigate('Feed') }
                    style={{
                        padding: 15,
                        alignSelf: 'center',
                        backgroundColor: '#9806a8',
                        borderRadius: 8,
                        marginVertical: 10,
                        width: 300,
                    }}
                >
                    <Text style={{
                        color: '#fff',
                        fontWeight: 'bold',
                        textAlign: 'center',
                    }}>My Home Screen</Text>
                </TouchableOpacity>

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    icon: {
        width: 24,
        height: 24,
    },
});
